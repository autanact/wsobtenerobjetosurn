
/**
 * WSObtenerObjetosURNSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSObtenerObjetosURNSkeleton java skeleton for the axisService
     */
    public class WSObtenerObjetosURNSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSObtenerObjetosURNLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param urns
                                     * @param nombreCamposResultado
         */
        

                 public co.net.une.www.gis.WSObtenerObjetosURNRSType obtenerObjetosURN
                  (
                  co.net.une.www.gis.Urns urns,java.lang.String nombreCamposResultado
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("urns",urns);params.put("nombreCamposResultado",nombreCamposResultado);
		try{
		
			return (co.net.une.www.gis.WSObtenerObjetosURNRSType)
			this.makeStructuredRequest(serviceName, "obtenerObjetosURN", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    