
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.svc;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WSObtenerObjetosURN-RQ-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WSObtenerObjetosURNRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "urns".equals(typeName)){
                   
                            return  co.net.une.www.gis.Urns.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WSObtenerObjetosURN-RS-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WSObtenerObjetosURNRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "Registros".equals(typeName)){
                   
                            return  co.net.une.www.gis.Registros.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "Dupla".equals(typeName)){
                   
                            return  co.net.une.www.gis.Dupla.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "Resultados".equals(typeName)){
                   
                            return  co.net.une.www.gis.Resultados.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "GisRespuestaGeneralType_GDE".equals(typeName)){
                   
                            return  co.net.une.www.gis.GisRespuestaGeneralType_GDE.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    